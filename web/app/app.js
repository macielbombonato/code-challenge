'use strict';

'use strict';
angular.module('app',
  [
    'ngRoute',
    'ngAnimate',
    'angularMoment',
    'angular-preload-image',
    'pascalprecht.translate',
    'truncate',
    'app.routes',
    'app.core',
    'app.services',
    'app.config',
    'app.translate'
  ]
);
