'use strict';

angular
    .module('app.services')
    .factory('UserService', dataService);

function dataService($http, $log, moment, BaseService) {
  var data = {
    'login': login,
    'list': list,
    'get': get,
    'createUser': createUser,
    'editUser': editUser,
    'deleteUser': deleteUser,
    'candidates': candidates,
    'voteUp': voteUp
  };

  function login(entity) {
    return BaseService.post('user/login', entity);
  }

  function list(apiToken, pageNumber) {
    var result = null;
    if (pageNumber != null && pageNumber != undefined) {
      result = BaseService.getWithKey('user/list/' + pageNumber, apiToken).then(function(response) {
        return response;
      });
    } else {
      result = BaseService.getWithKey('user/list', apiToken).then(function(response) {
        return response;
      });
    }

    return result;
  }

  function get(apiToken, id) {
    return BaseService.getWithKey('user/' + id, apiToken).then(function(response) {
      return response.user;
    });
  }

  function createUser(apiToken, entity) {
    return BaseService.postWithKey('user', entity, apiToken);
  }

  function editUser(apiToken, entity) {
    return BaseService.putWithKey('user', entity, apiToken);
  }

  function deleteUser(apiToken, id) {
    return BaseService.deleteWithKey('user/' + id, apiToken);
  }

  function candidates() {
    return BaseService.get('user/candidates');
  }

  function voteUp(candidateId, humanCheckEnum, humanCheckValue) {
    var candidate = {
      id: candidateId,
      humanCheck: humanCheckEnum,
      humanCheckValue: humanCheckValue
    }

    return BaseService.post('user/vote-up', candidate);
  }

  return data;

  function dataServiceError(errorResponse) {
      $log.error('XHR Failed for ShowService');
      $log.error(errorResponse);
      return errorResponse;
  }
}
