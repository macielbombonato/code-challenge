'use strict';

angular
    .module('app.services')
    .factory('BaseService', dataService);

function dataService($http, ServiceValue, $log, moment) {
  var data = {
    'get': get,
    'getWithKey': getWithKey,
    'post': post,
    'postWithKey': postWithKey,
    'putWithKey': putWithKey,
    'deleteWithKey': deleteWithKey
  };

  function get(url) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'GET',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      },
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  function getWithKey(url, apiToken) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'GET',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'key': apiToken
      },
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  function post(url, entity) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'POST',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      },
      'data': entity,
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  function postWithKey(url, entity, apiToken) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'POST',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'key': apiToken
      },
      'data': entity,
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  function putWithKey(url, entity, apiToken) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'PUT',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'key': apiToken
      },
      'data': entity,
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  function deleteWithKey(url, apiToken) {
    var requestUrl = ServiceValue.BASE_URL + url;

    return $http({
      'url': requestUrl,
      'method': 'DELETE',
      'headers': {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'key': apiToken
      },
      'cache': false
    }).then(function(response){
        return response.data;
    }).catch(dataServiceError);
  }

  return data;

  function dataServiceError(errorResponse) {
    $log.error('XHR Failed for BaseService');
    $log.error(errorResponse);
    return errorResponse;
  }
}
