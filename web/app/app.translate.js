'use strict';

angular
  .module('app.translate', ['pascalprecht.translate'])
  .config(translateConfig);

function translateConfig ($translateProvider) {
  // App Translation
  $translateProvider.useStaticFilesLoader({
    prefix : 'assets/i18n/',
    suffix : '.json'
  });

  $translateProvider.preferredLanguage('pt_BR');
  $translateProvider.usePostCompiling(true);
  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  $translateProvider.fallbackLanguage([
    'pt-BR'
  ]);
}
