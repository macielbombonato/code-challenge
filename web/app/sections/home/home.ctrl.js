'use strict';
angular
  .module('app.core')
  .controller('HomeController',
    function($scope, PageValues, $translate, UserService) {
    //Set page title and description
    PageValues.title = $translate.instant('home.title');
    PageValues.description = $translate.instant('home.description');

    //Setup view model object
    var vm = this;

    vm.candidateList = [];
    vm.candidate = {};

    UserService.candidates().then(function(response) {
      vm.candidateList = response;
    });

    $scope.voteUp = function(id, humanCheckEnum) {
      UserService.voteUp(id, humanCheckEnum, $scope.home.candidate.humanCheckValue).then(function(response) {
        if (response.status == 500) {
          $scope.voteSuccess = false;
          $scope.voteError = true;

          UserService.candidates().then(function(response) {
            vm.candidateList = response;
          });
        } else {
          vm.candidateList = response;

          $scope.voteSuccess = true;
          $scope.voteError = false;

          UserService.candidates().then(function(response) {
            vm.candidateList = response;
          });
        }

        $scope.home.candidate.humanCheckValue = null;

      });
    }

  }
);
