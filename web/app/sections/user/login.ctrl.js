'use strict';
angular
  .module('app.core')
  .controller('LoginController',
    function($scope, $rootScope, $location, PageValues, $translate, UserService) {
    //Set page title and description
    PageValues.title = $translate.instant('user.login.title');
    PageValues.description = $translate.instant('user.login.description');

    //Setup view model object
    var vm = this;

    vm.principal = {};

    vm.login = function() {
      UserService.login($scope.login.principal).then(function(response) {
        if (response.user != undefined && response.user != null) {
          $rootScope.principal = response.user;
          $rootScope.principal.authenticated = true;
          $location.path('/home');
        } else {
          vm.loginMessage = $translate.instant('user.login.error_message');
        }
      });
    }

  }
);
