'use strict';
angular
  .module('app.core')
  .controller('UserController',
    function($scope, $rootScope, $location, $routeParams, PageValues, $translate, UserService) {
    //Set page title and description
    PageValues.title = $translate.instant('user.list.title');
    PageValues.description = $translate.instant('user.list.description');

    //Setup view model object
    var vm = this;

    vm.users = [];
    $scope.isReadonly = true;

    vm.list = function() {
      UserService.list($rootScope.principal.apiToken, $routeParams.pageNumber).then(function(response) {
        vm.users = response;

        if ($routeParams.pageNumber != undefined && $routeParams.pageNumber != null) {
          vm.users.currentIndex = $routeParams.pageNumber;
        } else {
          vm.users.currentIndex = 1;
        }

        vm.users.firstUrl = '#/user/list/' + 1;

        if (vm.users.currentIndex > 1) {
          vm.users.prevUrl = '#/user/list/' + (vm.users.currentIndex - 1);
        }

        if (vm.users.currentIndex < vm.users.totalPages) {
          vm.users.nextUrl = '#/user/list/' + (vm.users.currentIndex + 1);
        }

        vm.users.lastUrl = '#/user/list/' + vm.users.totalPages;

        vm.users.pages = [];
        for (var i = 1; i <= vm.users.totalPages; i ++) {
          var page = {};
          page.index = i;
          page.url = '#/user/list/' + i;

          vm.users.pages.push(page);
        }
      });

      $scope.isReadonly = true;
    }

    vm.show = function() {
      UserService.get($rootScope.principal.apiToken, $routeParams.id).then(function(response) {
        vm.user = response;
      });
    }

    vm.createForm = function() {
      vm.user = {};
      $scope.isReadonly = false;
    }

    vm.create = function() {
      UserService.createUser($rootScope.principal.apiToken, vm.user).then(function(response) {
        vm.list();
        $location.path('/user');
      });
    }

    vm.editForm = function() {
      vm.show();
      $scope.isReadonly = false;
    }

    vm.edit = function() {
      UserService.editUser($rootScope.principal.apiToken, vm.user).then(function(response) {
        vm.list();
        $location.path('/user');
      });
    }

    vm.delete = function(id) {
      UserService.deleteUser($rootScope.principal.apiToken, id).then(function(response) {
        vm.list();
        $location.path('/user');
      });
    }

  }
);
