'use strict';

angular
  .module('app.routes', ['ngRoute'])
  .config(config);

function config ($routeProvider) {
  $routeProvider.
    when('/', {
      templateUrl: 'sections/home/home.tpl.html',
      controller: 'HomeController as home'
    })
    .when('/login', {
      templateUrl: 'sections/user/login.tpl.html',
      controller: 'LoginController as login'
    })
    .when('/user/list', {
      templateUrl: 'sections/user/list.tpl.html'
    })
    .when('/user/list/:pageNumber', {
      templateUrl: 'sections/user/list.tpl.html'
    })
    .when('/user/:id/edit', {
      templateUrl: 'sections/user/edit.tpl.html'
    })
    .when('/user/create', {
      templateUrl: 'sections/user/create.tpl.html'
    })
    .otherwise({
      redirectTo: '/'
    });
}
