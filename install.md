# CODE Challenge
---------

Este processo foi testado em uma máquina ubuntu linux 14.04 padrão e limpa da Digital Ocean.

No terminal digite os seguintes comandos

  sudo apt-get update
  sudo apt-get install git -y
  cd
  git clone https://bitbucket.org/macielbombonato/entelgy-code-challenge.git
  cd entelgy-code-challenge
  ./install.sh

Após ter terminado a instalação (e alterado os arquivos indicados pelo processo de instalação), execute a chamada **install** que consta [nesta coleção do postman](https://www.getpostman.com/collections/37819e51a10bc36edbe5) que disponibilizei ou siga o modelo indicado na parte de API descrita em **README.md**.
