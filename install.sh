sudo apt-get update

sudo apt-get install mysql-server -y
sudo apt-get install openjdk-7-jdk -y

sudo update-alternatives --config java

sudo apt-get install tomcat7 -y

sudo apt-get install maven -y

source ~/.profile

cd entelgy-code-challenge

git pull

cd api

mvn clean install

cd api/target
sudo service tomcat7 stop
rm -rf /var/lib/tomcat7/webapps/ROOT
mv bombonato-api-1.0.0.war ROOT.war
cp ROOT.war /var/lib/tomcat7/webapps
sudo service tomcat7 start

sudo apt-get install apache2 -y
sudo apt-get install nodejs -y
sudo apt-get install build-essential -y
sudo apt-get install nodejs-legacy
sudo apt-get install npm -y
sudo npm install -g bower@1.7.7

cd
cd entelgy-code-challenge
cd web
cd app
sudo bower install -f --allow-root
cp -R * /var/www/html

read -p "Não esqueça de alterar no arquivo /var/www/html/services/service.val.js o caminho para a api. http://ip:8080/api/"

echo "
export ENTELGY_SECRET_KEY=\"1234567890ABCDEF\" #max 16 chars
export ENTELGY_IV_KEY=\"1234567890ABCDEF\" #max 16 chars
export ENTELGY_DATASOURCE_DRIVER_CLASS=\"com.mysql.jdbc.Driver\"
export ENTELGY_HIBERNATE_DIALECT=\"org.hibernate.dialect.MySQL5InnoDBDialect\"
export ENTELGY_HIBERNATE_HBM2DDL=\"update\"
export ENTELGY_DATASOURCE_URL=\"jdbc:mysql://localhost:3306/bombonato_entelgy?createDatabaseIfNotExist=true&amp;useUnicode=true&amp;characterEncoding=utf-8\"
export ENTELGY_DATASOURCE_USERNAME=\"root\"
export ENTELGY_DATASOURCE_PASSWORD=\"root\"
export ENTELGY_HIBERNATE_SHOW_AND_FORMAT_SQL=\"false\"

"
read -p "Inclua as variáveis de ambiente listadas acima no arquivo /etc/init.d/tomcat7 logo após set -e"
read -p "Execute o comando sudo service tomcat7 restart "
