package bombonato.config;

import bombonato.business.model.ApplicationProperties;
import bombonato.business.service.UserService;
import bombonato.business.service.impl.UserServiceImpl;
import bombonato.common.util.ApoloCrypt;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.data.repository.UserRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@Configuration
public class AppTestConfig {

    @Bean
    public ApoloCrypt getApoloCrypt() {
        ApoloCrypt result = new ApoloCrypt();

        return result;
    }

    @Bean
    public ApplicationProperties getApplicationProperties() {
        ApplicationProperties result = new ApplicationProperties();

        result.setIvKey("1234567890ABCDEF");
        result.setSecretKey("1234567890ABCDEF");

        return result;
    }

    @Bean
    public UserRepository getUserRepository() {
        UserRepository result = Mockito.mock(UserRepository.class);

        User userAdmin = new User();
        userAdmin.setId(1L);
        userAdmin.setName("Maciel Bombonato");
        userAdmin.setEmail("maciel.bombonato@gmail.com");
        userAdmin.setStatus(UserStatus.ADMIN);

        User userActive = new User();
        userActive.setId(2L);
        userActive.setName("Maciel Bombonato + Active");
        userActive.setEmail("maciel.bombonato+active@gmail.com");
        userActive.setStatus(UserStatus.ACTIVE);

        User userLocked = new User();
        userLocked.setId(3L);
        userLocked.setName("Maciel Bombonato + Locked");
        userLocked.setEmail("maciel.bombonato+locked@gmail.com");
        userLocked.setStatus(UserStatus.LOCKED);

        List<User> userList = new ArrayList<User>();
        userList.add(userAdmin);
        userList.add(userActive);
        userList.add(userLocked);

        List<User> userListAdmin = new ArrayList<User>();
        userListAdmin.add(userAdmin);

        Page<User> userPage = Mockito.mock(Page.class);
        when(userPage.getTotalElements()).thenReturn(3L);
        when(userPage.getTotalPages()).thenReturn(1);
        when(userPage.getContent()).thenReturn(userList);

        Page<User> userPageAdmin = Mockito.mock(Page.class);
        when(userPageAdmin.getTotalElements()).thenReturn(1L);
        when(userPageAdmin.getTotalPages()).thenReturn(1);
        when(userPageAdmin.getContent()).thenReturn(userListAdmin);

        PageRequest request = new PageRequest(1 - 1, 100, Sort.Direction.ASC, "name");

        when(result.count()).thenReturn(3L);

        when(result.findByEmail("maciel.bombonato@gmail.com")).thenReturn(userAdmin);
        when(result.findByEmail("maciel.bombonato+active@gmail.com")).thenReturn(userActive);

        when(result.findByToken("123")).thenReturn(userAdmin);

        when(result.save(userAdmin)).thenReturn(userAdmin);

        when(result.findById(1L)).thenReturn(userAdmin);
        when(result.findOne(1L)).thenReturn(userAdmin);

        when(result.findByUserStatus(UserStatus.ACTIVE)).thenReturn(userList);

        when(result.findAll(request)).thenReturn(userPage);

        when(result.findAll()).thenReturn(userList);

        when(result.findByUserStatusNot(UserStatus.LOCKED, request)).thenReturn(userPage);

        when(result.findByUserStatus(UserStatus.LOCKED, request)).thenReturn(userPage);

        when(result.findByUserStatus(UserStatus.ADMIN, request)).thenReturn(userPageAdmin);

        when(result.findByUserStatus(UserStatus.ADMIN)).thenReturn(userList);

        String param = "maciel";

        when(
                result.findByNameLikeOrEmailLikeAndUserStatusNotOrderByNameAsc(
                        param,
                        param,
                        UserStatus.ADMIN,
                        request
                )
        ).thenReturn(userPageAdmin);

        when(
                result.findByNameLikeOrEmailLikeOrderByNameAsc(
                        param,
                        param,
                        request
                )
        ).thenReturn(userPage);

        when(
                result.findByUserStatusNotOrderByNameAsc(
                        UserStatus.LOCKED
                )
        ).thenReturn(userList);

        when(
                result.findByUserStatusNotOrderByNameAsc(
                        UserStatus.LOCKED,
                        request
                )
        ).thenReturn(userPage);

        return result;
    }

    @Bean
    public UserService getUserService() {
        UserService result = new UserServiceImpl();

        return result;
    }


}
