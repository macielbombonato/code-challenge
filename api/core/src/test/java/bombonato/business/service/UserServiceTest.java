package bombonato.business.service;

import bombonato.business.model.ApplicationProperties;
import bombonato.config.AppTestConfig;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class UserServiceTest {

    @Inject
    private UserService userService;

    @Inject
    private ApplicationProperties applicationProperties;

    @Test
    public void testFindByLoginSuccess() {
        User user = userService.findByLogin("maciel.bombonato@gmail.com");

        assertNotNull(user);
        assertEquals("Maciel Bombonato", user.getName());
    }

    @Test
    public void testFindByLoginError() {
        User user = userService.findByLogin("email nao cadastrado");

        assertNull(user);
    }

    @Test
    public void testSaveSuccess() {
        User userAdmin = new User();
        userAdmin.setId(1L);
        userAdmin.setName("Maciel Bombonato");
        userAdmin.setEmail("maciel.bombonato@gmail.com");
        userAdmin.setStatus(UserStatus.ADMIN);

        userAdmin = userService.save(userAdmin);

        assertNotNull(userAdmin.getId());
    }

    @Test
    public void testListSuccess() {
        List<User> userList = userService.list();

        assertNotNull(userList);
        assertEquals(3, userList.size());
    }

    @Test
    public void testListWithParamSuccess() {
        Page<User> userPage = userService.list(1);

        assertNotNull(userPage);
        assertEquals(3, userPage.getTotalElements());
        assertEquals(1, userPage.getTotalPages());
    }

    @Test
    public void testGetSystemAdminstratorSuccess() {
        User user = userService.getSystemAdministrator();

        assertNotNull(user);
        assertEquals(new Long(1L), user.getId());
        assertEquals(UserStatus.ADMIN, user.getStatus());
    }

    @Test
    public void testVoteUpSuccess() {
        User user = userService.voteUp(1L);

        assertNotNull(user);
        assertEquals(new Long(1L), user.getId());
        assertEquals(UserStatus.ADMIN, user.getStatus());
        assertEquals(new Long(1L), user.getVotes());
    }

}
