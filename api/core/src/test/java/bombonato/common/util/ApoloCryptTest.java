package bombonato.common.util;

import bombonato.business.model.ApplicationProperties;
import bombonato.config.AppTestConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.inject.Inject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppTestConfig.class, loader = AnnotationConfigContextLoader.class)
public class ApoloCryptTest {

    @Inject
    private ApoloCrypt apoloCrypt;

    @Inject
    private ApplicationProperties applicationProperties;

    @Test
    public void testEncode() {
        String encoded = apoloCrypt.encode("maciel");
        String decoded = apoloCrypt.decode(encoded);
        assertEquals("maciel", decoded);
        assertEquals("ec2ae74b6f69c0dac0c03e39bf1aecba", encoded);
    }

    @Test
    public void testEncodeParams() {
        String encoded = null;
        String decoded = null;
        try {
            encoded = apoloCrypt.encode("maciel", applicationProperties.getSecretKey(), applicationProperties.getIvKey());
            decoded = apoloCrypt.decode(encoded, applicationProperties.getSecretKey(), applicationProperties.getIvKey());
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals("maciel", decoded);
        assertEquals("ec2ae74b6f69c0dac0c03e39bf1aecba", encoded);
        assertNotNull(encoded);
        assertNotNull(decoded);
    }

}
