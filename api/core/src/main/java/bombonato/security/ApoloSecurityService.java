package bombonato.security;

import bombonato.business.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("apoloSecurity")
public class ApoloSecurityService {

    private static final Logger log = LoggerFactory.getLogger(ApoloSecurityService.class);

    @Inject
    private UserService userService;

    public boolean isAuthenticated(String key) {
        boolean result = false;

        if (userService.getAuthenticatedUser(key) != null) {
            result = true;
        }

        return result;
    }

}
