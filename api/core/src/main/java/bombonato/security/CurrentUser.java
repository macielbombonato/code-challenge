package bombonato.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class CurrentUser extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 8790909411747968450L;

	private Long id;

	private String name;
	
	private bombonato.data.model.User systemUser;

	public CurrentUser(
				Long id, 
				String username, 
				String password, 
				bombonato.data.model.User systemUser
			) {

		super(systemUser, password);
		this.id = id;
		this.name = username;
		this.systemUser = systemUser;
		super.setDetails(systemUser);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public bombonato.data.model.User getSystemUser() {
		return systemUser;
	}

	public void setSystemUser(bombonato.data.model.User systemUser) {
		this.systemUser = systemUser;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
