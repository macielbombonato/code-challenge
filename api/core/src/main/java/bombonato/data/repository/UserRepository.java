package bombonato.data.repository;

import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
	
	Page<User> findByUserStatusNotOrderByNameAsc(
			UserStatus userStatus,
			Pageable page
		);

	List<User> findByUserStatusNotOrderByNameAsc(
			UserStatus userStatus
	);

	Page<User> findByNameLikeOrEmailLikeOrderByNameAsc(
			String name,
			String email,
			Pageable page
		);

	Page<User> findByNameLikeOrEmailLikeAndUserStatusOrderByNameAsc(
			String name,
			String email,
			UserStatus userStatus,
			Pageable page
		);

	Page<User> findByNameLikeOrEmailLikeAndUserStatusNotOrderByNameAsc(
			String name,
			String email,
			UserStatus userStatus,
			Pageable page
		);

	Page<User> findByUserStatus(
			UserStatus userStatus,
			Pageable page
		);

	Page<User> findByUserStatusNot(
			UserStatus userStatus,
			Pageable page
		);

	Page<User> findAll(
			Pageable page
		);

	List<User> findByUserStatus(UserStatus userStatus);
	
	User findByEmailAndPassword(String email, String password);

	User findByResetPasswordToken(String token);
	
	User findById(Long id);

	long count();

	User findByToken(String token);

}
