package bombonato.business.service;

import bombonato.business.service.base.BaseService;
import bombonato.data.model.User;
import org.springframework.data.domain.Page;

public interface UserService extends BaseService<User> {

	final int PAGE_SIZE = 100;

	User findByLogin(String login);

	User save(String serverUrl, User user, boolean changePassword);

	Page<User> search(Integer pageNumber, String param);

	User lock(User user);

	User unlock(User user);

	Page<User> list(Integer pageNumber);

	Page<User> listLocked(Integer pageNumber);

	User getSystemAdministrator();

	boolean systemSetup(String serverUrl, User user);

	User loadByUsernameAndPassword(String username, String password);

	User find(Long id);

	int increaseSignInCounter(User user);

	long count();

	void generateResetPasswordToken(String serverUrl, String email);

	User findByToken(String token);

	User findByResetToken(String token);

	void reconstructAuthenticatedUser(User user);

	User voteUp(Long id);
}