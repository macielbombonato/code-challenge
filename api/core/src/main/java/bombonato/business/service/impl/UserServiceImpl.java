package bombonato.business.service.impl;

import bombonato.business.model.ApplicationProperties;
import bombonato.business.service.UserService;
import bombonato.common.exception.AccessDeniedException;
import bombonato.common.exception.BusinessException;
import bombonato.common.util.ApoloCrypt;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.data.repository.UserRepository;
import bombonato.security.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	@Inject
	private UserRepository userRepository;

	@Inject
	private ApplicationProperties applicationProperties;

	@Inject
	private ApoloCrypt apoloCrypt;

	public List<User> list() {
		List<User> result = userRepository.findByUserStatusNotOrderByNameAsc(
				UserStatus.LOCKED
		);

		return result;
	}

	public User find(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public int increaseSignInCounter(User user) {
		int result = 0;

		if (user != null) {
			if (user.getSignInCount() != null) {
				user.setSignInCount(user.getSignInCount() + 1);
			} else {
				user.setSignInCount(1);
			}

			String token = null;

			try {
				token = apoloCrypt.encode(
						user.getEmail() + new Date().getTime(),
						user.getEmail(),
						applicationProperties.getIvKey()
				);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

			user.setToken(token);

			user.setLastSignInAt(user.getCurrentSignInAt());

			user.setCurrentSignInAt(new Date());

			result = user.getSignInCount();

			if (user.getResetPasswordToken() != null) {
				user.setResetPasswordToken(null);
				user.setResetPasswordSentAt(null);
			}

			userRepository.save(user);
		}

		return result;
	}

	@Override
	public long count() {
		long result = 0L;

		result = userRepository.count();

		return result;
	}

	@Override
	public void generateResetPasswordToken(String serverUrl, String email) {
		if (email != null
				&& !"".equals(email)) {
			User user = this.findByLogin(email);

			if (user != null) {
				try {
					String token = apoloCrypt.encode(
							user.getEmail(),
							user.getEmail(),
							applicationProperties.getIvKey()
					);

					user.setResetPasswordToken(token);
					user.setResetPasswordSentAt(new Date());

					user = this.save(user);

				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public User findByToken(String token) {
		User user = null;

		user = userRepository.findByToken(token);

		return user;
	}

	@Override
	public User findByResetToken(String token) {
		User user = null;

		user = userRepository.findByResetPasswordToken(token);

		if (user != null
				&& user.getResetPasswordSentAt() != null) {
			Calendar now = GregorianCalendar.getInstance();
			now.setTime(new Date());

			Calendar resetAt = GregorianCalendar.getInstance();
			resetAt.setTime(user.getResetPasswordSentAt());
			resetAt.add(Calendar.MINUTE, 60);

			if (resetAt.before(now)) {
				user.setResetPasswordToken(null);
				user.setResetPasswordSentAt(null);

				this.save(user);

				user = null;
			}
		} else {
			user = null;
		}

		return user;
	}

	public User findByLogin(String login) {
		return userRepository.findByEmail(login);
	}

	public User loadByUsernameAndPassword(String username, String password) {
		User user = null;

		try {
			user = userRepository.findByEmailAndPassword(
					username,
					apoloCrypt.encode(password)
			);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			String message = "Password conversion error";
			throw new BusinessException(message);
		}

		if (user == null) {
			String message = "User not found";
			throw new UsernameNotFoundException(message);
		}

		return user;
	}

	@Transactional
	public User save(User entity) {
		return save(null, entity, false);
	}

	@Transactional
	public User save(String serverUrl, User user, boolean changePassword) {
		if (UserStatus.ADMIN.equals(user.getStatus())) {
			User systemAdmin = getSystemAdministrator();

			if (systemAdmin.getId() != user.getId()) {
				String message = "Only an administrator can change an administrator user.";
				throw new AccessDeniedException(message);
			}
		}

		boolean sendEmail = false;

		if (user.getId() == null) {
			try {
				String token = apoloCrypt.encode(
						user.getEmail(),
						user.getEmail(),
						applicationProperties.getIvKey()
				);

				user.setResetPasswordToken(token);
				user.setResetPasswordSentAt(new Date());

				sendEmail = true;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}

		if (changePassword) {
			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			try {
				user.setPassword(
						apoloCrypt.encode(user.getPassword())
				);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				String message = "Password conversion error.";
				throw new BusinessException(message);
			}
		} else {
			User dbUser = this.find(user.getId());
			user.setPassword(dbUser.getPassword());
		}

		user = userRepository.save(user);

		return user;
	}

	@Transactional
	public void remove(User user) {
		if (!user.getStatus().isChangeable()) {
			String message = "Administrator can't be deleted.";
			throw new BusinessException(message);
		}

		userRepository.delete(user);
	}

	public Page<User> search(Integer pageNumber, String param) {
		if (pageNumber < 1) {
			pageNumber = 1;
		}

		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "name");

		if (param != null) {
			param = "%" + param + "%";
		}

		Page<User> result = userRepository.findByNameLikeOrEmailLikeOrderByNameAsc(
				param,
				param,
				request
		);

		return result;
	}

	public User lock(User user) {
		if (!user.getStatus().isChangeable()) {
			String message = "Administrator can't be locked.";
			throw new AccessDeniedException(4, message);
		}

		User dbUser = this.find(user.getId());
		user.setPassword(dbUser.getPassword());
		user.setStatus(UserStatus.LOCKED);

		return userRepository.save(user);
	}

	public User unlock(User user) {
		if (!user.getStatus().isChangeable()) {
			String message = "Administrator can't be locked.";
			throw new AccessDeniedException(4, message);
		}

		User dbUser = this.find(user.getId());
		user.setPassword(dbUser.getPassword());
		user.setStatus(UserStatus.ACTIVE);

		return userRepository.save(user);
	}

	public Page<User> listLocked(Integer pageNumber) {
		if (pageNumber < 1) {
			pageNumber = 1;
		}

		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "name");

		return userRepository.findByUserStatus(UserStatus.LOCKED, request);
	}

	public Page<User> list(Integer pageNumber) {
		if (pageNumber < 1) {
			pageNumber = 1;
		}

		PageRequest request = new PageRequest(pageNumber - 1, PAGE_SIZE, Sort.Direction.ASC, "name");

		return userRepository.findByUserStatusNot(UserStatus.LOCKED, request);
	}

	public User getSystemAdministrator() {
		User user = null;

		List<User> userList = userRepository.findByUserStatus(UserStatus.ADMIN);

		if (userList != null && !userList.isEmpty()) {
			user = userList.get(0);
		}

		return user;
	}

	@Transactional
	public boolean systemSetup(String serverUrl, User user) {
		boolean result = false;

		User systemAdmin = this.getSystemAdministrator();

		if (user != null
				&& systemAdmin == null) {

			boolean sendEmail = false;

			try {
				String token = apoloCrypt.encode(
						user.getEmail(),
						user.getEmail(),
						applicationProperties.getIvKey()
				);

				user.setResetPasswordToken(token);
				user.setResetPasswordSentAt(new Date());

				sendEmail = true;
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

			Md5PasswordEncoder encoder = new Md5PasswordEncoder();
			try {
				user.setPassword(
						apoloCrypt.encode(user.getPassword())
				);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				String message = "Password conversion error.";
				throw new BusinessException(message);
			}

			user = userRepository.save(user);
		}

		return result;
	}

	private StringBuilder buildResetPasswordMessage(String url, User user) {
		StringBuilder result = new StringBuilder();

		result.append("<html>");
		result.append("<body>");

		result.append("<h1>");
		result.append("Change password");
		result.append("</h1>");

		result.append("<p>");
		result.append("Did you forget your password? If yes, click or copy the link below, if not, don't worry, no action is needed.");
		result.append("</p>");

		result.append("<p>");

		result.append("<a href=\"" + url + user.getResetPasswordToken() + "\" >");
		result.append(url + user.getResetPasswordToken());
		result.append("</a>");

		result.append("</p>");

		result.append("</body>");
		result.append("</html>");

		return result;
	}

	private StringBuilder buildCreateUserMessage(String url, User user) {
		StringBuilder result = new StringBuilder();

		result.append("<html>");
		result.append("<body>");

		result.append("<h1>");
		result.append("New account");
		result.append("</h1>");

		result.append("<p>");
		result.append("Hi, " + user.getName() + ", your access account has been successfully created. Now you need to reset your password before you start. To do this, click or copy the link below.");
		result.append("</p>");

		result.append("<p>");

		result.append("<a href=\"" + url + user.getResetPasswordToken() + "\" >");
		result.append(url + user.getResetPasswordToken());
		result.append("</a>");

		result.append("</p>");

		result.append("</body>");
		result.append("</html>");

		return result;
	}

	public void reconstructAuthenticatedUser(User user) {
		boolean isLoged = true;

		if (user == null) {
			user = new User();
			isLoged = false;
		}

		Authentication newAuth = new CurrentUser(
				user.getId(),
				user.getEmail(),
				user.getPassword(),
				user
		);

		SecurityContextHolder.getContext().setAuthentication(newAuth);

		if (!isLoged) {
			SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
		}
	}

	@Override
	public User voteUp(Long id) {
		User result = this.find(id);

		if (result.getVotes() == null) {
			result.setVotes(1L);
		} else {
			result.setVotes(result.getVotes() + 1L);
		}

		result = userRepository.save(result);

		return result;
	}
}
