package bombonato.business.service.impl;

import bombonato.business.service.base.BaseService;
import bombonato.data.model.User;
import bombonato.data.model.base.BaseEntity;
import bombonato.data.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

@SuppressWarnings("rawtypes")
public abstract class BaseServiceImpl<E extends BaseEntity> implements BaseService<E> {
	
	protected static final Logger LOG = LoggerFactory.getLogger(BaseService.class);

	@Inject
	private UserRepository userRepository;
	
	public User getAuthenticatedUser(String key) {
		User user = null;
		
		try {
			if (key != null
					&& !"".equals(key)) {
				user = userRepository.findByToken(key);
			}

		} catch(Throwable errorId) {
			LOG.error(errorId.getMessage(), errorId);
		}
		
		return user;
	}
	
}
