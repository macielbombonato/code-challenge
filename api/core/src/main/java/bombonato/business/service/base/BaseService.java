package bombonato.business.service.base;

import bombonato.data.model.User;
import bombonato.data.model.base.BaseEntity;

import java.util.List;

@SuppressWarnings("rawtypes")
public interface BaseService<E extends BaseEntity> {
	
	E find(Long id);
	
	E save(E entity);
	
	void remove(E entity);
	
	User getAuthenticatedUser(String key);
	
	List<E> list();
	
}
