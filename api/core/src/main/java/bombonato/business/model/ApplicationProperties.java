package bombonato.business.model;

public class ApplicationProperties {
	
	private String secretKey;

	private String ivKey;

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getIvKey() {
		return ivKey;
	}

	public void setIvKey(String ivKey) {
		this.ivKey = ivKey;
	}
}
