package bombonato.business.model;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.Locale;
import java.util.Map;

public enum Language {

	BR("pt_BR") {
		@Override
		public Locale getLocale() {
			return new Locale("pt", "BR");
		}
	},
	
	US("en_US") {
		@Override
		public Locale getLocale() {
			return new Locale("en", "US");
		}
	},
	
	ES("es_ES") {
		@Override
		public Locale getLocale() {
			return new Locale("es", "ES");
		}
	},
	
	;
	
	private final String code;
	
	private static final Map<String, Language> valueMap;
	
	static {
		Builder<String, Language> builder = ImmutableMap.builder();
		for (Language type : values()) {
			builder.put(type.code, type);
		}
		valueMap = builder.build();
	}
	
	public static Language fromCode(String code) {
		return valueMap.get(code);
	}

	private Language(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	public abstract Locale getLocale();
	
}
