package bombonato.web.apimodel;

import bombonato.web.apimodel.base.BaseAPIModel;

import java.util.List;

public class CandidateList extends BaseAPIModel {

    private long totalVotes;

    private List<Candidate> candidates;

    public long getTotalVotes() {
        return totalVotes;
    }

    public void setTotalVotes(long totalVotes) {
        this.totalVotes = totalVotes;
    }

    public List<Candidate> getCandidates() {
        return candidates;
    }

    public void setCandidates(List<Candidate> candidates) {
        this.candidates = candidates;
    }
}
