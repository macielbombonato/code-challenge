package bombonato.web.apimodel;

import bombonato.data.model.User;
import bombonato.web.apimodel.base.BaseAPIModel;

public class UserAPI extends BaseAPIModel {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
