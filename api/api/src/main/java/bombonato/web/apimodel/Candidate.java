package bombonato.web.apimodel;

import bombonato.web.apimodel.base.BaseAPIModel;
import bombonato.web.enums.HumanValidation;

public class Candidate extends BaseAPIModel {

    private Long id;

    private String name;

    private String email;

    private Long votes;

    private Long humanCheckValue;

    private HumanValidation humanCheck;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getVotes() {
        return votes;
    }

    public void setVotes(Long votes) {
        this.votes = votes;
    }

    public HumanValidation getHumanCheck() {
        return humanCheck;
    }

    public void setHumanCheck(HumanValidation humanCheck) {
        this.humanCheck = humanCheck;
    }

    public Long getHumanCheckValue() {
        return humanCheckValue;
    }

    public void setHumanCheckValue(Long humanCheckValue) {
        this.humanCheckValue = humanCheckValue;
    }
}
