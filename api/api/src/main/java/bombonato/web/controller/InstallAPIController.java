package bombonato.web.controller;

import bombonato.business.model.ApplicationProperties;
import bombonato.business.service.UserService;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.web.apimodel.AuthUser;
import bombonato.web.apimodel.UserAPI;
import bombonato.web.controller.base.BaseAPIController;
import bombonato.web.service.UserAuthenticationProvider;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(value = "/install")
public class InstallAPIController extends BaseAPIController<User> {

    @Inject
    private UserService userService;

    @Inject
    private ApplicationProperties applicationProperties;

    @Inject
    private UserAuthenticationProvider userAuthenticationProvider;


    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            value = "",
            method = RequestMethod.POST
    )
    public @ResponseBody
    UserAPI login(
            @RequestBody AuthUser entity,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            UserAPI result = new UserAPI();

            User user = null;

            if (entity != null
                    && entity.getUsername() != null) {

                user = new User();

                user.setStatus(UserStatus.ADMIN);

                user.setName(entity.getUsername());
                user.setEmail(entity.getUsername());
                user.setPassword(entity.getPassword());

                userService.systemSetup(getServerUrl(request), user);

            } else {
                result.setMessage("Username can't be null.");
                response.setStatus(403);
            }

            return result;
        } catch (Throwable th) {
            log.error(th.getMessage(), th);

            UserAPI result = new UserAPI();
            result.setMessage(th.getMessage());

            response.setStatus(500);
            return result;
        }
    }

}
