package bombonato.web.controller;

import bombonato.business.service.UserService;
import bombonato.common.exception.AccessDeniedException;
import bombonato.common.exception.BusinessException;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.security.CurrentUser;
import bombonato.web.apimodel.*;
import bombonato.web.controller.base.BaseAPIController;
import bombonato.web.enums.HumanValidation;
import bombonato.web.service.UserAuthenticationProvider;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/user")
public class UserAPIController extends BaseAPIController<User> {

    @Inject
    private UserService userService;

    @Inject
    private UserAuthenticationProvider userAuthenticationProvider;

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            value = "login",
            method = RequestMethod.POST
    )
    public @ResponseBody
    UserAPI login(
            @RequestBody AuthUser entity,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            UserAPI result = new UserAPI();

            User user = null;

            if (entity != null
                    && entity.getUsername() != null) {

                Authentication authentication = new CurrentUser(null, entity.getUsername(), entity.getPassword(), null);

                authentication = userAuthenticationProvider.authenticate(authentication);

                if (authentication != null) {
                    user = (User) authentication.getPrincipal();

                    result.setUser(user);
                    response.setStatus(200);
                }
            } else {
                result.setMessage("Username can't be null.");
                response.setStatus(403);
            }

            return result;
        } catch (Throwable th) {
            log.error(th.getMessage(), th);

            UserAPI result = new UserAPI();
            result.setMessage(th.getMessage());

            response.setStatus(500);
            return result;
        }
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "list",
            method = RequestMethod.GET
    )
    public @ResponseBody
    UserList listFirstPage(
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        return this.list(1, request, response);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "list/{pageNumber}",
            method = RequestMethod.GET
    )
    public @ResponseBody
    UserList listSomePage(
            @PathVariable Integer pageNumber,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        return this.list(pageNumber, request, response);
    }

    private UserList list(
            Integer pageNumber,
            HttpServletRequest request,
            HttpServletResponse response
    ) {

        UserList result = new UserList();

        if (checkAccess(result, request)) {
            Page<User> page = userService.list(pageNumber);

            if (page != null) {
                result.setTotalPages(page.getTotalPages());
                result.setTotalElements(page.getTotalElements());

                if (page.getContent() != null
                        && !page.getContent().isEmpty()) {
                    result.setUserList(page.getContent());

                    response.setStatus(200);
                } else {
                    response.setStatus(204);
                }
            }
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "list-all/{pageNumber}",
            method = RequestMethod.GET
    )
    public @ResponseBody
    UserList listAllSomePage(
            @PathVariable Integer pageNumber,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        return this.list(pageNumber, request, response);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "{id}",
            method = RequestMethod.GET
    )
    public @ResponseBody
    UserAPI find(
            @PathVariable Long id,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        UserAPI result = new UserAPI();

        if (checkAccess(result, request)) {
            User requestUser = getUserFromRequest(request);

            User user = userService.find(id);

            if (user != null) {
                result.setUser(user);

                response.setStatus(200);
            } else {
                response.setStatus(204);
            }
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            value = "",
            method = RequestMethod.POST
    )
    public @ResponseBody
    UserAPI create(
            @RequestBody UserFormModel entity,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        UserAPI result = new UserAPI();

        if (checkAccess(result, request)) {
            if (entity != null
                    && entity.getEmail() != null) {
                User user = userService.findByLogin(entity.getEmail());

                if (user != null) {
                    result.setMessage("User account already exist.");

                    response.setStatus(203);
                } else {
                    user = new User();
                    user.setName(entity.getName());
                    user.setEmail(entity.getEmail());
                    user.setMobile(entity.getMobile());
                    user.setPassword(entity.getPassword());

                    user.setStatus(UserStatus.ACTIVE);
                }

                if (entity.getPassword() == null
                        || "".equals(entity.getPassword())) {
                    entity.setPassword("123mudar");
                }

                user = userService.save(
                        getServerUrl(request),
                        user,
                        true
                    );

                result.setUser(user);
                response.setStatus(201);

            } else {
                response.setStatus(500);
            }
        } else {
            response.setStatus(500);
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            value = "",
            method = RequestMethod.PUT
    )
    public @ResponseBody
    UserAPI update(
            @RequestBody UserFormModel entity,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        UserAPI result = new UserAPI();

        if (checkAccess(result, request)) {
            boolean hasChangePassword = false;

            if (entity != null
                    && entity.getId() != null) {
                hasChangePassword = false;

                if (entity.getPassword() != null
                        && !"".equals(entity.getPassword())) {
                    hasChangePassword = true;
                }

                User dbEntity = userService.find(entity.getId());

                if (dbEntity != null) {
                    dbEntity.setName(entity.getName());
                    dbEntity.setEmail(entity.getEmail());

                    if (entity.getMobile() != null
                            && !"".equals(entity.getMobile())) {
                        dbEntity.setMobile(entity.getMobile());
                    }

                    if (hasChangePassword) {
                        dbEntity.setPassword(entity.getPassword());
                    }

                    User requestUser = getUserFromRequest(request);

                    dbEntity = userService.save(
                            getServerUrl(request),
                            dbEntity,
                            hasChangePassword
                    );

                    if (dbEntity != null) {
                        result.setUser(dbEntity);

                        response.setStatus(200);
                    } else {
                        response.setStatus(203);
                    }
                } else {
                    response.setStatus(204);
                }
            } else {
                response.setStatus(204);
            }
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "{id}",
            method = RequestMethod.DELETE
    )
    public @ResponseBody
    UserAPI delete(
            @PathVariable Long id,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        UserAPI result = new UserAPI();

        if (checkAccess(result, request)) {
            User user = userService.find(id);

            if (user != null) {
                if (UserStatus.ADMIN.equals(user.getStatus())) {
                    throw new AccessDeniedException(9, "You can remove an administrator.");
                }

                userService.remove(user);

                result.setUser(null);

                response.setStatus(200);
            } else {
                result.setMessage("Not deleted");

                response.setStatus(203);
            }
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            value = "candidates",
            method = RequestMethod.GET
    )
    public @ResponseBody
    CandidateList candidates(
            HttpServletRequest request,
            HttpServletResponse response
    ) {

        CandidateList result = new CandidateList();

        List<User> userList = userService.list();

        if (userList != null
                && !userList.isEmpty()) {

            result.setCandidates(new ArrayList<Candidate>());

            long totalVotes = 0L;
            Candidate candidate = null;

            int index = 0;

            for (User user : userList) {
                candidate = new Candidate();

                // Without ID the system can't do upvote
                if (user.getId() != null) {
                    candidate.setId(user.getId());
                } else {
                    continue;
                }

                if (user.getVotes() != null
                        && user.getVotes() > 0L) {
                    totalVotes += user.getVotes();

                    candidate.setVotes(user.getVotes());
                } else {
                    candidate.setVotes(0L);
                }

                candidate.setName(user.getName());
                candidate.setEmail(user.getEmail());

                if (index >= HumanValidation.values().length) {
                    index = 0;
                }

                candidate.setHumanCheck(HumanValidation.values()[index++]);

                result.getCandidates().add(candidate);
            }

            result.setTotalVotes(totalVotes);

            response.setStatus(200);
        } else {
            response.setStatus(204);
        }

        return result;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            value = "vote-up",
            method = RequestMethod.POST
    )
    public @ResponseBody
    Candidate voteUp(
            @RequestBody Candidate candidate,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        if (candidate != null
                && candidate.getId() != null) {

            try {
                if (candidate.getHumanCheck() != null
                        && candidate.getHumanCheck().getResult().equals(candidate.getHumanCheckValue())) {
                    User user = userService.voteUp(candidate.getId());

                    if (user.getId() != null) {
                        candidate.setId(user.getId());
                    }

                    if (user.getVotes() != null
                            && user.getVotes() > 0L) {
                        candidate.setVotes(user.getVotes());
                    } else {
                        candidate.setVotes(0L);
                    }

                    candidate.setName(user.getName());
                    candidate.setEmail(user.getEmail());

                    candidate.setHumanCheck(HumanValidation.values()[0]);

                    response.setStatus(200);
                } else {
                    throw new BusinessException("Validation Error");
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                throw new BusinessException("Validation Error");
            }
        }

        return candidate;
    }

}
