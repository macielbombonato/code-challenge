package bombonato.web.controller.base;

import bombonato.business.service.UserService;
import bombonato.data.model.base.BaseEntity;
import com.google.gson.annotations.SerializedName;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public abstract class BaseController<E extends BaseEntity> {

    protected static final Logger log = LoggerFactory.getLogger(BaseController.class);

    @Inject
    protected UserService userService;

    protected JSONObject jsonParser(Object obj) throws IllegalArgumentException, IllegalAccessException, JSONException {
        JSONObject object = new JSONObject();

        Class<?> objClass = obj.getClass();
        Field[] fields = objClass.getDeclaredFields();
        for(Field field : fields) {
            field.setAccessible(true);
            Annotation[] annotations = field.getDeclaredAnnotations();
            for(Annotation annotation : annotations){
                if(annotation instanceof SerializedName){
                    SerializedName myAnnotation = (SerializedName) annotation;
                    String name = myAnnotation.value();
                    Object value = field.get(obj);

                    if(value == null)
                        value = new String("");

                    object.put(name, value);
                }
            }
        }

        return object;
    }

    protected String getServerUrl(HttpServletRequest request) {
        String serverUrl = "";

        try {
            URL url = new URL(request.getRequestURL().toString());

            String host  = url.getHost();
            String userInfo = url.getUserInfo();
            String scheme = url.getProtocol();
            int port = url.getPort();
            String path = (String) request.getAttribute("javax.servlet.forward.request_uri");
            String query = (String) request.getAttribute("javax.servlet.forward.query_string");
            URI uri = new URI(scheme,userInfo,host,port,path,query,null);

            serverUrl = uri.toString() + "/";

        } catch (MalformedURLException e) {
            log.error(e.getMessage(), e);
        } catch (URISyntaxException e) {
            log.error(e.getMessage(), e);
        }

        return serverUrl;
    }

}
