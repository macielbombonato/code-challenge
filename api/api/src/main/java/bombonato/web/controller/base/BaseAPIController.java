package bombonato.web.controller.base;

import bombonato.business.service.UserService;
import bombonato.common.exception.AccessDeniedException;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.data.model.base.BaseEntity;
import bombonato.security.ApoloSecurityService;
import bombonato.web.apimodel.base.BaseAPIModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.MediaType;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class BaseAPIController<E extends BaseEntity> extends BaseController<E> {

    protected static final Logger log = LoggerFactory.getLogger(BaseAPIController.class);

    @Inject
    private ApoloSecurityService apoloSecurity;

    @Inject
    private UserService userService;

    protected boolean checkAccess(
            BaseAPIModel model,
            HttpServletRequest request) {

        boolean hasAccess = false;

        User user = getUserFromRequest(request);

        if (user != null) {

            if (!UserStatus.LOCKED.equals(user.getStatus())) {
                hasAccess = true;
            } else {
                hasAccess = false;

                throw new AccessDeniedException("Access Denied");
            }
        }

        if (!hasAccess) {
            model.setMessage("Access Denied");

            throw new AccessDeniedException(model.getMessage());
        }

        return hasAccess;
    }

    protected User getUserFromRequest(HttpServletRequest request) {
        String apiKey = request.getHeader("key");

        User result = null;

        if (apiKey != null) {
            result = userService.findByToken(apiKey);
        }

        return result;
    }

    @ExceptionHandler(Exception.class)
    public @ResponseBody
    BaseAPIModel handleException(
            Exception ex,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        log.error(ex.getMessage(), ex);

        BaseAPIModel model = new BaseAPIModel();

        model.setMessage("Internal Server Error");

        response.setStatus(500);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        return model;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public @ResponseBody
    BaseAPIModel handleExceptionResourceNotFound(
            ResourceNotFoundException ex,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        log.error(ex.getMessage(), ex);

        BaseAPIModel model = new BaseAPIModel();

        model.setMessage("Resource Not Found");

        response.setStatus(404);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        return model;
    }

    @ExceptionHandler(AccessDeniedException.class)
    public @ResponseBody
    BaseAPIModel handleExceptionAccessDenied(
            AccessDeniedException ex,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        log.error(ex.getMessage(), ex);

        BaseAPIModel model = new BaseAPIModel();

        model.setMessage(ex.getCustomMsg());

        response.setStatus(ex.getPrincipalCode());
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        return model;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        //Create a custom binder that will convert a String with pattern dd/MM/yyyy to an appropriate Date object.
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

        binder.registerCustomEditor(Date.class, "createdAt", new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class, "updatedAt", new CustomDateEditor(dateFormat, false));

        binder.registerCustomEditor(Date.class, "resetPasswordSentAt", new CustomDateEditor(dateFormat, false));

        binder.registerCustomEditor(Date.class, "currentSignInAt", new CustomDateEditor(dateFormat, false));
        binder.registerCustomEditor(Date.class, "lastSignInAt", new CustomDateEditor(dateFormat, false));

    }

}
