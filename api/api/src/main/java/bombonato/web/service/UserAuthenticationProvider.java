package bombonato.web.service;

import bombonato.business.model.ApplicationProperties;
import bombonato.business.service.UserService;
import bombonato.common.exception.AccessDeniedException;
import bombonato.data.enums.UserStatus;
import bombonato.data.model.User;
import bombonato.security.CurrentUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component("userAuthenticationProvider")
public class UserAuthenticationProvider implements AuthenticationProvider {

	private static final Logger log = LoggerFactory.getLogger(UserAuthenticationProvider.class);

	@Inject
	private UserService userService;

	@Inject
	private ApplicationProperties applicationProperties;

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		User user = null;

		String username = authentication.getName();

		if (username != null
				&& !"".equals(username)) {

			user = userService.loadByUsernameAndPassword(
					username,
					authentication.getCredentials().toString()
			);
		}

		if (user != null
				&& user.getId() != null) {
			if (UserStatus.LOCKED.equals(user.getStatus())) {
				String message = "This user is locked. Please, contact system adminstrator.";
				throw new BadCredentialsException(message, new AccessDeniedException(0, message));
			}

			// Increase the access count
			try {
				user.setSignInCount(userService.increaseSignInCounter(user));
			} catch (Throwable e) {
				log.error("********* => Error when try to count the login sequence");
				log.error(e.getMessage(), e);
			}

			return new CurrentUser(
					user.getId(),
					user.getEmail(),
					user.getPassword().toLowerCase(),
					user
			);
		} else {
			throw new BadCredentialsException("Access Denied");
		}

	}

	@SuppressWarnings("rawtypes")
	public boolean supports(Class authentication) {
		return (UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication));
	}
}
