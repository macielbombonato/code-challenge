package bombonato.web.enums;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.Map;

public enum HumanValidation {
	TWO_PLUS_THREE("TWO_PLUS_THREE") {
		@Override
		public Long getResult() {
			return 5L;
		}
	},
	TWO_TIMES_THREE("TWO_TIMES_THREE") {
		@Override
		public Long getResult() {
			return 6L;
		}
	},
	FOUR_DIVED_ONE("FOUR_DIVED_ONE") {
		@Override
		public Long getResult() {
			return 4L;
		}
	};

	private final String code;

	private static final Map<String, HumanValidation> valueMap;

	static {
		Builder<String, HumanValidation> builder = ImmutableMap.builder();
		for (HumanValidation tipo : values()) {
			builder.put(tipo.code, tipo);
		}
		valueMap = builder.build();
	}

	public static HumanValidation fromCode(String code) {
		return valueMap.get(code);
	}

	private HumanValidation(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public abstract Long getResult();
	
}
