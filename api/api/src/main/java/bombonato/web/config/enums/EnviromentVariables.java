package bombonato.web.config.enums;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.Map;

public enum EnviromentVariables {

	DATASOURCE_DRIVER_CLASS("ENTELGY_DATASOURCE_DRIVER_CLASS"),
	DATASOURCE_URL("ENTELGY_DATASOURCE_URL"),
	DATASOURCE_USERNAME("ENTELGY_DATASOURCE_USERNAME"),
	DATASOURCE_PASSWORD("ENTELGY_DATASOURCE_PASSWORD"),

	HIBERNATE_DIALECT("ENTELGY_HIBERNATE_DIALECT"),
	HIBERNATE_HBM2DDL("ENTELGY_HIBERNATE_HBM2DDL"),
	HIBERNATE_SHOW_AND_FORMAT_SQL("ENTELGY_HIBERNATE_SHOW_AND_FORMAT_SQL"),

	SECRET_KEY("ENTELGY_SECRET_KEY"),
	IV_KEY("ENTELGY_IV_KEY"),

	;

	private final String code;

	private static final Map<String, EnviromentVariables> valueMap;

	static {
		Builder<String, EnviromentVariables> builder = ImmutableMap.builder();
		for (EnviromentVariables type : values()) {
			builder.put(type.code, type);
		}
		valueMap = builder.build();
	}

	public static EnviromentVariables fromCode(String code) {
		return valueMap.get(code);
	}

	private EnviromentVariables(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
