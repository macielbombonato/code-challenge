# CODE Challenge
---------
## Início do projeto

Comecei o projeto utilizando como base um projeto pessoal que tenho no seguinte endereço: [Projeto Apolo](https://github.com/macielbombonato/apolo)

A ideia deste projeto é ter algum lugar para colocar código para testes/estudar e também ter alguma coisa meio pronta sempre que eu precise de algo meio padrão e rápido.

Para este desafio, peguei a ideia estrutural do Apolo mudando algumas coisas na base, conforme lista abaixo:

- O projeto Apolo foi feito para rodar em uma estrutura multi-tenant com base de dados única. Para este desafio, retirei isso;
- O sistema de permissões de acesso é aquele que comentei com vcs durante a entrevista (alterando o mecanismo do Spring Secutiry para aceitar enums) e de forma bem granular (uma permissão para listagem, outra para criação, isso tudo incluído em grupos de permissão. O usuário é associado a estes grupos. Como o projeto Apolo é para um propósito geral, fazendo este mecanismo bem granular, acabo tendo algo que possa atender de tudo). Aqui, não acredito que exista a necessidade de tanto, então, deixei apenas um enum de status no usuário (ativo e bloqueado) e este enum tem a opção ADMIN que pode servir para o prósito de super usuário (apesar de não estar sendo utilizado para nada no projeto, ficou apenas para constar);
- Por conta dessas alterações, e como estou montando a aplicação sob a visão de api acessada por um aplicativo angular, retirei as validações de segurança do Spring e estou fazendo apenas uma checagem simples se o token do usuário é válido e com isso, sigo o fluxo da aplicação;

Algumas coisas não vi por que mudar, pois acho que podem agregar ao sistema e o modelo do Apolo é bem básico, então, acaba ficando como era mesmo.

- Classe para criptografar Strings (senha, tokens, etc...);
- O esquema de configuração do ambiente foi praticamente mantido na íntegra. Coloquei tudo em variáveis de ambiente, chaves de criptografia, configurações do banco de dados e a configuração para envio de emails (Só pra constar, o Apolo também envia pelo Mandrill, então, caso consultem ele verão que tb existe essa implementação, outro detalhe, é que este mecanismo de envio de email pelo Mandrill retorna um objeto com campos que fazem validações se o email foi enviado, lido e aí por diante. Com o envio SMTP não faço essas validações. Para o Apolo, acabei usando elas quando montei um sistema de envio de newsletter [Hermes](http://hermes.wab.com.br/));

Abaixo estão as variáveis de ambiente que devem ser configuradas no servidor da aplicação.

### Variáveis de ambiente

	export ENTELGY_SECRET_KEY="1234567890ABCDEF" #max 16 chars
	export ENTELGY_IV_KEY="1234567890ABCDEF" #max 16 chars

	export ENTELGY_DATASOURCE_DRIVER_CLASS="com.mysql.jdbc.Driver"
	export ENTELGY_HIBERNATE_DIALECT="org.hibernate.dialect.MySQL5InnoDBDialect"
	export ENTELGY_HIBERNATE_HBM2DDL="update"
	export ENTELGY_DATASOURCE_URL="jdbc:mysql://localhost:3306/bombonato_entelgy?createDatabaseIfNotExist=true&amp;useUnicode=true&amp;characterEncoding=utf-8"
	export ENTELGY_DATASOURCE_USERNAME="root"
	export ENTELGY_DATASOURCE_PASSWORD="root"
	export ENTELGY_HIBERNATE_SHOW_AND_FORMAT_SQL="false"


## Instalação

Leia o arquivo **install.md** e acompanhe os passos feitos em **install.sh**.

Para atualização do ambiente após ter executado o **install.sh**, execute o **run.sh**, nele não inclui os apt-get install, apenas detalhes de atualização de ambiente.

Os testes integrados da API foram feitos via postman (chrome plugin) e para que o sistema funcione, é necessário criar o usuário que administra o sistema (aquele que criará outros usuários e estes podem cadastrar os candidatos).

Considerando um acesso local, execute a seguinte URL:

	http://localhost:8080/install
	
Seguindo a estrutura descrita abaixo nos detalhes da API.

## API

Abaixo estão os métodos disponíveis na API do sistema.

Todas as operações da API permitem **cross domain origin**.

As operações que não requerem usuário autenticado possuirão isso em sua descrição, caso contrário, a ação necessita que o **header key** seja enviado na requisição.

### Install

	POST /install
	Content-Type: application/json

**Exemplo de json para postar**

	{
		"username":"Nome do Usuário",
		"password":"senha do usuário"
	}
	
Quando o sistema é instalado, o banco de dados está vazio e a aplicação não possui nenhum usuário administrador.

Para resolver este problema, basta executar esta chamada da API que ela irá criar um usuário administrador do sistema, este será o passo inicial para cadastrar os demais usuários/candidatos.

Esta operação não deve retornar dados em caso de sucesso e ela funcionará apenas se o sistema ainda não tiver um administrador cadastrado na base.

Não é necessário enviar a chave do usuário.

### Login

	POST /user/login
	Content-Type: application/json

**Exemplo de json para postar**

	{
		"username":"Nome do Usuário",
		"password":"senha do usuário"
	}

Este serviço irá retornar o registro do usuário autenticado. É necessário recuperar o token deste retorno, ele será utilizado em todas as outras chamadas preenchendo o campo "key" do cabeçalho.

Não é necessário enviar a chave do usuário.

### Listar usuários

	GET /user/list
	GET /user/list/{pageNumber}

Retorna uma lista de usuários paginada. O objeto de retorno informa o número total de usuários cadastrados e o total de páginas que aquela consulta pode retornar.

### Visualizar um usuário específico

	GET /user/{id}
	Content-Type: application/json
	
Retorna o registro do usuário referente ao ID informado.

### Criar usuário

	POST /user
	Content-Type: application/json

**Exemplo de json para postar**

	{
		"name":"Seu Nome",
		"email":"seu@email.com",
		"mobile": "111",
		"password": "123"
	}

### Editar usuário

	PUT /user
	Content-Type: application/json

**Exemplo de json para postar**

	{
		"name":"Seu Nome",
		"email":"seu@email.com",
		"mobile": "111",
		"password": "123"
	}

Somente os campos indicados no json acima poderão ser editados, os demais campos do usuário não podem ser alterados pela API.

### Excluir usuário

	DELETE /user/{id}
	
### Listar candidatos

	GET /user/candidates
	Content-Type: application/json

Retorna a lista completa de candidatos (que são usuários cadastrados no sistema). O objeto de retorno informa a quantidade total de votos computados para que os percentuais sejam calculados sem a necessidade de percorrer a lista várias vezes desnecessáriamente.

Incluí neste retorno um enumerador que representa uma continha simples. Este enumerador foi colocado aqui como uma solução caseira ao captcha.

A ideia disso foi fazer uma validação de se quem está votando é humano. No documento do desafio foi mencionado um grande número de requisições simultâneas, portanto, fiz dessa forma para evitar a inclusão de mais uma requisição externa (lendo a documentação do captcha, vi que é preciso realizar uma requisição server side para validar se o usuário validou o formulário corretamente), por isso, acreditei que neste caso este recurso poderia atrapalhar no quesito performance.

Tentei pensar em algo melhor e mais difícil de burlar (sim, esse método que coloquei está bem falho), mas, por enquanto, só me ocorreu este método.

Não é necessário enviar a chave do usuário.

### Registrar voto

	POST /user/vote-up
	Content-Type: application/json

**Exemplo de json para postar**

	{
		"id":"id do usuário",
		"humanCheck":"Enumerador associado ao candidato no momento da consulta"
		"humanCheckValue":"resultado da equação informada no formulário"
	}
	
Esta operação irá registrar seu voto para o candidato referente ao ID informado.

Não é necessário enviar a chave do usuário.

### Observações sobre a API

Vocês vão notar que a listagem de usuários está sendo feita através da action **list**. 

Coloquei desta forma por conta da paginação.

Achei que seria mais intuitivo para quem usa a **api** ver algo assim do que colocar o GET em /user e colocar o paginador como variável no path.

Outro detalhe da API e este é bem importante, é em relação ao login.
Esta solução está bem simples ainda e na minha opinião tem muito que melhorar, por exemplo, armazenar o IP e session ID do usuário para evitar logins multiplos e também dificultar invasão através do uso apenas da chave do usuário.

Outro ponto é que estou usando a data para gerar a chave, mas ainda não foi implementado a validação desta data para que esta chave passe a ter uma validade de fato, hoje, se o usuário ficar um mês sem fazer login e usar a mesma chave, o sistema funciona e isso é bem ruim.

## Sistema Web

Para a interface WEB, achei que seria mais interessante utilizar uma abordagem com AngularJS, justificando assim o desenvolvimento de uma API REST ao invés de uma aplicação WEB convencional.

### Primeiros passos

As dependências da aplicação são mantidas via bower, portando, é necessário possuir o **bower** na máquina que está gerando o pacote da aplicação para que as dependências sejam baixadas.

Vá até a pasta **app** e digite o seguinte comando:

	bower install
	
Isso deverá gerar a pasta **bower_components** dentro da pasta assets. Nesta pasta estarão dependências como angular, jquery e assim por diante.

Feito isso, o pacote está pronto para execução, basta que o conteúdo da pasta **app** seja incluído no diretório de seu servidor de aplicação para que a aplicação passe a trabalhar, por exemplo, se você possui o **php** instalado em sua máquina local, dentro da pasta **app** execute o seguinte comando:

	php -S 0.0.0.0:3000
	
Isso fará com que a aplicação seja executada na porta 3000. Fica bem fácil para testes locais.

Outra abordagem é incluir os artefatos da pasta **app** na pasta alvo do servidor apache e assim por diante.

Esta abordagem do apache é a realizada pelo processo de instalação.

### A escolhda da plataforma e interface.

Como eu havia dito na entrevista, sou novo com Angular, tenho apenas um projeto desenvolvido profissionalmente ([Poptracking](http://poptracking.com.br/)), que foi desenvolvido com AngularJS rodando em [Meteor](https://www.meteor.com/) utilizando o template [Angle](http://wrapbootstrap.com/preview/WB04HF123). 

Ainda estou experimentando o Angular e para este aplicativo do desafio estou utilizando como base arquitetural o modelo [angularjs-by-example](http://revillweb.github.io/angularjs-by-example/#/) pois achei a estrutura dele bacana e estou me desenvolvendo utilizando ela como base.

Como este "template" possui uma interface que achei visualmente agradável, acabei adotando os estilos deles também, editando apenas alguns pequenos pontos.

Das mudanças que fiz, um exemplo foi alterar as dependências de fixas para atualizadas via bower. Logo, não versiono dependências do sistema, é necessário executar um **bower install** sempre que o sistema é baixado em um computador.

### Funcionamento

Ao acessar o sistema, a home irá apresentar a lista de candidatos. Nesta, o usuário deverá clicar em Votar, isso irá abrir uma pequena caixa com uma conta simples, junto com o botão enviar. O usuário deve resolver a equação e clicar em enviar.

Caso a conta esteja correta, o sistema irá computar o voto, emitir uma mensagem de sucesso e atualizar a lista com os novos valores.

Caso a conta esteja incorreta, o sistema irá emitir uma mensagem de erro e atualizar a lista com os valores de votos atualizados (outros usuários podem ter votado naquele intervalo).

#### Know issue

Esse sistema de equação funciona do lado do servidor (por enquanto) da seguinte forma:

- A aplicação web envia para a API o enum que está associado ao candidato naquele momento e qual o resultado da conta que o usuário digitou;
- O servidor valida se o enum existe e caso sim, recupera dele o valor pré-definido como resultado;
- Caso o valor bata, o voto é computado;
- Caso o valor não esteja correto, o sistema emite uma mensagem de erro e não computa o voto;

O problema aqui é que se uma aplicação ficar fazendo pooling no servidor informando sempre um enum conhecido e o resultado correto de conta para ele, a aplicação computa os votos sempre. Logo, o sistema aqui está bem falho no quesito de validação de humanos.

OBS.: Montei os casos de teste assim.

Deixarei esta opção para o final, logo, se vocês estiverem lendo este texto, irão se deparar com este problema e acabei não tendo tempo para resolvê-lo.

## Teste integrados

Utilizei dois sistemas para realizar alguns testes de carga.

- SoapUI 5.0;
- Jmeter 2.13;

Os scripts que gerei para estes testes estão na pasta **tests** na raiz do projeto.

Um sitema para testes durante o desenvolvimento:

- Postman (plugin para chrome);

Criei alguns cenários de teste durante o desenvolvimento da API, estes podem ser importados [clicando aqui](https://www.getpostman.com/collections/37819e51a10bc36edbe5).


## Ambiente de testes web

Montei um servidor na Digital Ocean (máquina básica) com a aplicação pra ficar fácil de vocês verem como ficou.

É só [cllicar aqui](http://159.203.90.128).